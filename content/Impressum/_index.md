+++
title = "Impressum"
date = 2021-12-09T12:33:32+01:00
weight = 101
+++

### Authors

<a href="https://orcid.org/0000-0002-8021-5524" target="_blank" rel="noopener">
     <h4>Tobias Hamann </h4>
</a>
 - Chair of Production Metrology and Quality Management & 
Institute for Information Management in Mechanical Engineering (Prof. Robert Schmitt) of the RWTH Aachen University
<br>

<a href="https://orcid.org/0009-0003-6022-2633" target="_blank" rel="noopener">
    <h4>Jonas Werheid</h4>
</a>
 - Chair of Production Metrology and Quality Management & 
Institute for Information Management in Mechanical Engineering (Prof. Robert Schmitt) of the RWTH Aachen University
<br>

<a href="https://orcid.org/0000-0001-9325-4074" target="_blank" rel="noopener">
    <h4>Mario Moser</h4>
</a>
 - Chair of Production Metrology and Quality Management & 
Institute for Information Management in Mechanical Engineering (Prof. Robert Schmitt) of the RWTH Aachen University
<br>

<a id="Michéle Robrecht">
    <h4>Michéle Robrecht</h4>
</a>
 - Fraunhofer Institute for Production Technology IPT
<br>

<a id="Steffen Hillemacher">
    <h4>Steffen Hillemacher</h4>
</a>
 - Software Engineering, Department of Computer Science 3, RWTH Aachen
<br>

<a id="Pierre Kehl">
    <h4>Pierre Kehl</h4>
</a>
 - Fraunhofer Institute for Production Technology IPT
<br>

<a id="Sahar Ben-Hassine">
    <h4>Sahar Ben-Hassine</h4>
</a>
 - Industrial Information Technology, TU Berlin

### Acknowledgements

The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.