+++
title = "The Problem"
date = 2021-12-09T12:33:32+01:00
weight = 10
+++

There are plenty decisions to be taken a throughout RDM processes. What is the environment of the research project? What are the demands of my funding organisation, my university or even my institute? What tools should be used for the own research? How do i find this information? Where to start and what comes next?

![RDM is complex](/images/Problem_Complex.png "RDM is complex")

Researchers face such problems every day when getting in touch with research data mangement. Often, they don't know the specific requirements they have to match. Adding to that, universities often have rather vague RDM policies. This causes a lot of uncertainty amgonst researchers. 

