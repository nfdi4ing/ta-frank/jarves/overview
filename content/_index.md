+++
title = "Jarves"
date = 2021-12-09T12:33:32+01:00
weight = 1
+++

 # Jarves


 ## Joined Assistant in Research for Versatile Engineering Sciences

Jarves supports researchers of the engineering sciences in their everyday work. It focuses on the reduction of effort and time of RDM. This is achieved by addressing the following points:

### Guidance in RDM
By ordering the RDM activities based on their occurrence in the engineering research process, Jarves provides a structure for RDM in engineering.
		
### Decision Support
Based on the research’s general environment, like requirements of funding organisations or institutional boundaries, Jarves provides information on the next steps and available tools. 	

### Connecting beyond
As Jarves is based on the Python Django REST Framework, it offers a broad connectivity to other services, allowing for seamless (meta)data exchange.


## Get involved!
If you want to get in touch with the developers, just [write us a mail](mailto:t.hamann@wzl-mq.rwth-aachen.de?subject=[NFDI4Ing]%20Jarves:%20)!

Or, if you just want to keep up with news around Jarves, you can sign up for our [mailinglist](https://lists.tu-darmstadt.de/mailman/listinfo/nfdi4ing_taskarea_frank)!
