+++
title = "Our solution"
date = 2021-12-09T12:33:32+01:00
weight = 20
+++

Jarves general idea is the underlying framework, that allows researchers to easily manage their research data. The core of this goal is based on five pillars: 

1. A **guided RDM process** for engineering.
2. Easy **management of rules and guildelines** of a research project to reduce initial RDM effort.
3. A **desicion support system** for reseachers, addressing exactly the problems they are faceing in the right moment.
4. Support via **training materials** at the point of need.
5. Partial **automation** to reduce effort needed in everyday RDM.

This could will look like this:

![Management of Guidelines in a guided process with decision support enriched with training materials](/images/Jarves_Support_1.png "Management of Guidelines in a guided process with decision support enriched with training materials")

Along with this, Jarves tries to draw as much work as possible from researchers by partly automating RDM and synchronisation to other services.

![Jarves' conection to other tools](/images/Jarves_Support_2.png "Jarves' conection to other tools")

As a result, we are developing a web based RDM tool, that guides researchers through their RDM along their research while supporting them at the point of need.

![Jarves' idea](/images/Jarves_Idea.png "Jarves' idea")

