+++
title = "Meet Archetype Frank "
date = 2021-12-09T12:33:32+01:00
weight = 99
+++

![Frank_Icon](/images/Frank_Icon.png "Frank_Icon")

“Hello, I’m FRANK. I’m an engineer who works with a **range of different and heterogeneous data sources** - including the collection of data from test persons up to manufacturing networks.
One of the main challenges during my research process refers to the **synchronisation and access management of simultaneously and distributedly generated data**. My professional  background is mostly informed by production engineering, industrial engineering, ergonomics, business engineering, product design and mechanical design, automation engineering, process engineering, civil engineering and transportation science.” 

For this archetype, many simultaneously involved participants, end-user-devices and machines will generate different data types and dimensions. Many participants can either refer to many researchers working together or to many research objects, e.g. test persons. The generated data can be clustered into machine and device related or human related. Managing such different data types and dimensions from many participants is unique to FRANK.

[On this page](https://git.rwth-aachen.de/nfdi4ing/ta-frank/jarves/overview) you will find all the organizational points for TA Frank, the participants and other points.
Later down the line we will publish data and files to the project within this repository.

For an overview of the work items, please refer to [this epic](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/5) or the list below.

#### Sample data typical for Frank

Below, we collected data sets that are typical but not unique to FRANK. What is unique however, is the variety of different data sets included. *Please note: This list is work in progress.*

| Dataset | Description | Link to dataset | Link to Code | Link to publication |
| --- | --- | --- | --- | --- |
| Uncertainty Quantification based on Bayesian neural networks for Predictive Quality | ... | [Github](https://github.com/predictive-quality/bnn-example) | [Github](https://github.com/predictive-quality/bnn-example) | [doi.org/10.1007/978-3-031-07155-3_10](https://doi.org/10.1007/978-3-031-07155-3_10) |
| A survey on the dissemination and usage of research data management and related tools | ... | [ZENODO](https://doi.org/10.5281/zenodo.7645548) (Not published yet) | (Work in progress) | (Work in progress) |
| NFDI4Ing Conference 2023 | ... | [ZENODO](https://doi.org/10.5281/zenodo.7998655) | [NFDI4Ing GitLab](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/tree/main/2022%20-%20RWTH%20Aachen) | [ZENODO](https://doi.org/10.5281/zenodo.7998655) |
| Virtual Climatization | ... | (Work in progress) | (Work in progress) | (Work in progress) |
| ... | ... | ... | ... | ... |
<!-- | ... | ... | ... | ... | -->

#### There is more to FRANK

In terms of **different and heterogeneous data sources** that need to be synchronized, FRANK also views RDM from a meta level. 
This means, that there are many tools and services for RDM already in which researchers insert their data. This might cause trouble for researchers as information might not be available everywhere in the project, or worse, be available in multiple places but containing different information. 

To address this problem, the proposed framework Jarves focusses on the **synchronisation and access management of simultaneously and distributedly generated RDM data**. 