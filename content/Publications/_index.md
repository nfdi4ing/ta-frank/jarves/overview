+++
title = "Jarves' publications"
date = 2021-12-09T12:33:32+01:00
weight = 90
+++

To get an overview of the Jarves related publications, please refer to the list below:

### Scientific articles:
- [A survey on the dissemination and usage of research data management and related tools in German engineering sciences](https://preprints.inggrid.org/repository/view/26/) - Preliminary study on the needs of researchers in the German engineering sciences
- [Matching Data Life Cycle and Research Processes in Engineering Sciences](https://preprints.inggrid.org/repository/view/42/) - A proposd new process for RDM accompanying engineering research as foundation for Jarves guided RDM process

### Posters:
- [Jarves: CoRDI 2023](https://doi.org/10.5281/zenodo.8315364) - The poster for the 1st NFDI CoRDI Conference (12.09.2023, eng)

### Presentations:
- [Workflows ingenieurwissenschaftlicher Forschungsabläufe - Auszug des aktuellen Forschungsstands](https://zenodo.org/doi/10.5281/zenodo.7125570) - A new approach to the data lifecycle proposed by NFDI4Ing Task Area Frank for Jarves (29.09.2022, ger)
- [Jarves: Der digitale FDM-Assistent](https://doi.org/10.5281/zenodo.7642482 ) - Presentation of Jarves on the "Offenes FDM-Netzwerktreffen: Digitale Unterstützung im FDM" of the RWTH Aachen University in context of the Love Data Week (15.02.2023, ger)
- [Jarves: The digital RDM assistant](https://doi.org/10.5281/zenodo.7715683) - Presentation of Jarves - Joint Assistant for Research in Versatile Engineering Sciences on the "RDA 20th Plenary Meeting - Gothenburg (Hybrid)" of the Research Data Alliance (10.03.2023, eng)
- [Jarves: The Digital Data Steward for Engineering Science Research](https://zenodo.org/doi/10.5281/zenodo.8378834) - A presentation on Jarves on the NFDI4Ing conference 2023 (27.09.2023, eng)
- [Meet Jarves - The Joint Assistant for Research in Versatile Engineering Sciences](https://zenodo.org/doi/10.5281/zenodo.10839103) - Presentation of Jarves on the "FDM-Werkstatt: Into the RDM Toolbox" of ForschungsdatenNRW (19.03.2024, eng)
- [Jarves, the digital data steward from NFDI4Ing](https://zenodo.org/doi/10.5281/zenodo.11185297) - A presentation on Jarves on the 112th Bibliocon (05.06.2024, eng)
- [Jarves: More guidance and less effort in RDM](https://zenodo.org/doi/10.5281/zenodo.13747295) - A presentation about Jarves on the NFDI4Ing Conference 2024 (18.09.2024, eng)