+++
title = "Organisational: Task Area Frank "
date = 2021-12-09T12:33:32+01:00
weight = 100
+++


## Organizational information about NFDI4Ing TA Frank

 
### Participants and persons involved

#### Tasks according to proposal:
| TUB | IoP | IPT |
| --- | --- | --- |
| Creation and detailing of a data model for FDM | Creation and detailing of an FDM workflow | Describe publication process and identify important aspects |
| Validation of the data model | Identification, clustering and validation of technologies for this FDM process | Listing of selection tools for the publication site |
| Co-development of the backend | Co-development of the software tool | Co-development of software components, e.g. API for RDMO/Coscine |

### Measure-Plan
![Measure plan](/images/Frank_Milestone_Plan.png "The Measure Plan")

#### Measures according to proposal:
|  Task                         | Description                                                                                          | Responsibility     | Status        |
| ---                           | ---                                                                                                   | ---               | ---           |
| [**F-1**](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/1)                       | **Target process specification**                                                                      | **RWTH**          | **ongoing**       |
| &nbsp;  [F-1-1](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/3)                    | Assess existing RDM process frameworks                                                             | WZL    | done       |
| &nbsp;  [F-1-2](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/2)                    | Define tasks per each step proposed RDM process <br>and provide additional Info on those Steps     | RWTH   | ongoing      |
| &nbsp;  [F-1-3](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/4)                     | Deploy and test developed RDM process                                                              | RWTH   | on hold      |
| [**F-2**](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/6)                       | **Technological feasibility and decision-making**                                                     | **Alle** | **ongoing**       |
| &nbsp;  [F-2-1](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/7)                     | Evaluation of technologies/software fitting F-1-2                                                  | RWHT  | ongoing      |
| &nbsp;  [F-2-2](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/8)                     | Clustering of identified technologies on research methodology                                      | RWTH  | ongoing      |
| &nbsp;  [F-2-3](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/9)                     | Feasibility check and gap analysis                                                                 | RWTH  | ongoing      |
| &nbsp;  [F-2-4](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/10)                     | Identification of current collaboration barriers                                                   | Alle  | on hold      |
| [**F-3**](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/11)                        | **Design concept of an application program interface**                                                | **WZL, TUB, IPT** | **ongoing**       |
| &nbsp;  [F-3-1](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/12)                     | Identify interfacing/segmentation points between workflows <br>and technologies                    | WZL, TUB, IPT   | ongoing      |
| &nbsp;  [F-3-2](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/13)                     | Conceptionalise a unifying approach/framework to integrate technologies                            | WZL, TUB, IPT   | ongoing    |
| &nbsp;  [F-3-3](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/14)                     | Conceptionalise a role-based access framework                                                      | WZL, TUB   | on hold      |
| &nbsp;  [F-3-4](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/15)                     | Support domain-specific visualisation                                                              | WZL, TUB   | on hold      |
| [**F-4**](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/16)                        | **Incentivation of an active and interdisciplinary RDM use**                                          | **Alle** | **on hold**       |
| &nbsp;  [F-4-1](https://git.rwth-aachen.de/groups/nfdi4ing/ta-frank/jarves/-/epics/17)                     | Cosult Stakeholders <br>such as the German Academic Association for ProductionTechnology (WGP)     | Alle  | on hold      |


### Additional info

The NFDI4Ing proposal can be found [here](https://zenodo.org/record/4015201#.YxCcWHZByUk).


