+++
title = "The data model"
date = 2021-12-09T12:33:32+01:00
weight = 70
+++

### Our data model (WIP)

Jarves uses a relational data model for its database. This works very well with the Django based architecture of the backend resulting in many benefits such as code readability and simplicity as well as easier communication to the frontend and other services via the API.

![Legend](/images/2023Q1_UML_3.png "Legend")

![The Jarves Data Model](/images/2023Q1_UML_2.png "The Jarves Data Model")

![The Jarves Authentication Data Model](/images/2023Q1_UML_1.png "The Jarves Authentication Data Model")

*Tip: Open the images in a new tab to make them zoomable.*

